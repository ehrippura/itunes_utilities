//
//  main.m
//  control_console
//
//  Created by Tzu-Yi Lin on 2014/1/28.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h> // foundation
#include <getopt.h> // for option

#import "TNiTunesControler.h" // itunes controller

#define BEGIN_AUTORELEASING()   @autoreleasepool {
#define END_AUTORELEASING()     }

@import ScriptingBridge;

static bool exitApp = false;

void usage()
{
    fprintf(stdout, "Usage: itnsctl <arguments>\n");
    fprintf(stdout, "--playlist, -p: print library playlist\n");
    fprintf(stdout, "\n");
}

void printPlaylist()
{
    SBElementArray *playlists = [[TNiTunesControler sharedController] libraryPlaylists];
    for (iTunesPlaylist *playlist in playlists) {
        NSInteger count = [playlists indexOfObject:playlist];
        fprintf(stdout, "%ld. %s\n", count, [playlist.name cStringUsingEncoding:NSUTF8StringEncoding]);
    }
}

void executeCommand(unsigned int flag, NSInteger playlist)
{

}

int main(int argc, const char * argv[])
{

    static struct option long_option[] = {
        { "playlist", no_argument, NULL, 'p' },
        { "shuffle", optional_argument, NULL, 's' },
        { "party", no_argument, NULL, 't' },
        {NULL, 0, NULL, 0}
    };

    int opt = 0;
    unsigned workingFlag = 0;
    int selectedPlaylist = 0;

    BEGIN_AUTORELEASING()

    while ((opt = getopt_long(argc, (char *const*)argv, "ps", long_option, NULL)) != -1) {
        switch (opt) {
            case 'p':
                printPlaylist();
                break;

            case 's': {
                if (optarg != NULL) {
                }

                workingFlag |= 0x01;
            }
                break;

            default:
                usage();
        }
    }

    executeCommand(workingFlag, selectedPlaylist);

    END_AUTORELEASING();

    return 0;
}

