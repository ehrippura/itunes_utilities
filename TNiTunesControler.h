//
//  TNiTunesControler.h
//  itunes_notification
//
//  Created by Tzu-Yi Lin on 2014/1/29.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "iTunes.h"

// notification
extern NSString *iTunesTrackChangedNotification;

// player state
extern NSString *iTunesPlayerStatePlaying;
extern NSString *iTunesPlayerStatePause;
extern NSString *iTunesPlayerStateStop;

@interface TNiTunesControler : NSObject

@property (nonatomic, readonly) BOOL iTunesPlaying;

+ (instancetype)sharedController;

- (SBElementArray *)libraryPlaylists;
- (SBElementArray *)tracksForPlaylistName:(NSString *)name;

- (iTunesTrack *)currentPlayingTrack;


@end
