//
//  PSSoundEntity.h
//  itunes_notification
//
//  Created by Tzu-Yi Lin on 2014/2/1.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PSSoundEntity : NSManagedObject

@property (nonatomic, retain) NSString * album;
@property (nonatomic, retain) NSString * artist;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSDate * appendDate;
@property (nonatomic, retain) NSNumber * played;
@property (nonatomic, retain) NSNumber * identifier;
@property (nonatomic, retain) NSNumber * trackid;

@end
