//
//  PSSoundEntity.m
//  itunes_notification
//
//  Created by Tzu-Yi Lin on 2014/2/1.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import "PSSoundEntity.h"


@implementation PSSoundEntity

@dynamic album;
@dynamic artist;
@dynamic title;
@dynamic played;
@dynamic identifier;
@dynamic appendDate;
@dynamic trackid;

@end
