//
//  PSPreference.h
//  itunes_notification
//
//  Created by Tzu-Yi Lin on 2014/2/1.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//


#define PSPreferenceSelectedLibraryName     @"tw.eternalwind.partyshuffle.pref.selectedLibrary"
#define PSPreferenceNumberOfTrack           @"tw.eternalwind.partyshuffle.pref.numberOfTrack"
#define PSPreferenceNumberOfHistory         @"tw.eternalwind.partyshuffle.pref.numberOfHistory"
