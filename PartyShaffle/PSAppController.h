//
//  PSAppController.h
//  itunes_notification
//
//  Created by Tzu-Yi Lin on 2014/2/1.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSAppController : NSObject <NSTableViewDataSource, NSTableViewDelegate> {
    IBOutlet NSPopUpButton *_libraryList;
    IBOutlet NSPopUpButton *_numberOfSound;
    IBOutlet NSPopUpButton *_history;
    IBOutlet NSTextField *_infoLabel;
    IBOutlet NSTableView *_tableView;
    
    IBOutlet NSArrayController *_arrayController;
}

- (IBAction)libraryChanged:(id)sender;
- (IBAction)historyCountChange:(id)sender;
- (IBAction)goNextTrack:(id)sender;
- (IBAction)goPreviousTrack:(id)sender;
- (IBAction)tableDoubleAction:(id)sender;

@end
