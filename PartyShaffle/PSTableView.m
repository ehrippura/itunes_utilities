//
//  PSTableView.m
//  itunes_notification
//
//  Created by Tzu-Yi Lin on 2014/2/5.
//  Copyright (c) 2014年 EternalWind. All rights reserved.
//

#import "PSTableView.h"

@implementation PSTableView

- (void)keyDown:(NSEvent *)theEvent
{
    unichar c = [[theEvent charactersIgnoringModifiers] characterAtIndex:0];

    if (c == '\r')
        [[self target] performSelector:[self doubleAction] withObject:self afterDelay:0];

    [super keyDown:theEvent];
}

@end
