//
//  PSAppController.m
//  itunes_notification
//
//  Created by Tzu-Yi Lin on 2014/2/1.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import "PSAppController.h"
#import "TNiTunesControler.h"
#import "PSAppDelegate.h"
#import "PSSoundEntity.h"
#import "PSPreference.h"

#include <stdlib.h>

@import CoreData;

NSInteger *createPreferredRandomList(NSInteger base, NSInteger number)
{
    NSInteger *ret = malloc(sizeof(NSInteger) * number);

    if (base <= number) {
        for (int i = 0; i < number; i++)
            ret[i] = i;
    } else {
        for (int i = 0; i < number; i++)
            ret[i] = -1;

        for (int i = 0; i < number; i++) {
            NSInteger see = arc4random() % base;
            bool skip = false;

            for (int j = 0; j < number; j++) {
                if (ret[j] == see) {
                    skip = true;
                    break;
                }
            }

            if (!skip)
                ret[i] = see;
            else
                i--;
        }
    }

    return ret;
}

@interface PSAppController ()
- (void)playTrackForId:(NSNumber *)identifier;
- (void)appendTrackToBottom:(NSInteger)number;
- (void)restoreSession;
@end

@implementation PSAppController {
    __weak PSAppDelegate *_appDelegate;
    __weak PSSoundEntity *_playingEntity;

    NSInteger _currentHistoryCount;
    // for sort
    NSArray *_sort;
}

- (id)init
{
    self = [super init];
    if (self) {
        _sort = @[[NSSortDescriptor sortDescriptorWithKey:@"appendDate" ascending:YES]];
    }
    return self;
}

- (void)awakeFromNib
{
    TNiTunesControler *controller = [TNiTunesControler sharedController];

    // set application delegate
    _appDelegate = [[NSApplication sharedApplication] delegate];

    // update playlist menu
    SBElementArray *sources = [controller libraryPlaylists];
    for (iTunesPlaylist *playlist in sources) {
        iTunesESpK kind = playlist.specialKind;

        if (kind != iTunesESpKBooks && kind != iTunesESpKITunesU && kind != iTunesESpKLibrary &&
            kind != iTunesESpKMovies && kind != iTunesESpKTVShows)

            [_libraryList addItemWithTitle:playlist.name];
    }

    // set table double action and datasource
    [_tableView setDoubleAction:@selector(tableDoubleAction:)];
    [_tableView setTarget:self];
    [_tableView setDataSource:self];
    [_tableView setDelegate:self];

    // restore preferences
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    NSString *previousLibrary = [pref objectForKey:PSPreferenceSelectedLibraryName];
    NSString *numOfTrack = [pref objectForKey:PSPreferenceNumberOfTrack];
    NSString *historyNum = [pref objectForKey:PSPreferenceNumberOfHistory];

    [_numberOfSound selectItemWithTitle:numOfTrack ?: @"10"];
    [_history selectItemWithTitle:historyNum ?: @"5"];
    if (previousLibrary) {
        [_libraryList selectItemWithTitle:previousLibrary];
        // add observer for reloading
        [_arrayController addObserver:self forKeyPath:@"arrangedObjects" options:NSKeyValueObservingOptionNew context:nil];
    }

    // register notification
    NSDistributedNotificationCenter *
    des = [NSDistributedNotificationCenter defaultCenter];

    [des addObserver:self selector:@selector(iTunesUpdate:) name:iTunesTrackChangedNotification object:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([change count] > 0 && change[NSKeyValueChangeNewKey] != [NSNull null]) {
        [self restoreSession];
        [_arrayController removeObserver:self forKeyPath:@"arrangedObjects"];
    }
}

- (void)restoreSession
{
    // get history
    NSArray *array = [_arrayController arrangedObjects];

    if ([array count] != 0) {
        TNiTunesControler *controller = [TNiTunesControler sharedController];
        SBElementArray *tracks = [controller tracksForPlaylistName:[_libraryList titleOfSelectedItem]];

        _currentHistoryCount = 0;
        NSInteger tracksNeedAppend = 0;

        for (PSSoundEntity *entity in array) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(name == %@) AND (databaseID == %@)", entity.title, entity.trackid];
            NSArray *filtered = [tracks filteredArrayUsingPredicate:predicate];

            if ([filtered count] == 0) {
                if (![entity.played boolValue])
                    tracksNeedAppend++;

                [_appDelegate.managedObjectContext deleteObject:entity];
            } else {
                iTunesTrack *track = [filtered objectAtIndex:0];
                entity.identifier = @(track.id);
                if ([entity.played boolValue]) {
                    _currentHistoryCount++;
                } else {
                    if (controller.iTunesPlaying) {
                        iTunesTrack *playingTrack = controller.currentPlayingTrack;
                        if ([entity.trackid integerValue] == playingTrack.databaseID)
                            _playingEntity = entity;
                    }
                }
            }
        }

        [_tableView reloadData];
        if (tracksNeedAppend != 0)
            [self appendTrackToBottom:tracksNeedAppend];
    }
    else [self libraryChanged:_libraryList];
}

/// action message
// refresh list
- (IBAction)libraryChanged:(id)sender
{
    NSString *name = [_libraryList titleOfSelectedItem];
    NSArray *tracks = [[TNiTunesControler sharedController] tracksForPlaylistName:name];

    NSInteger selectLimit = [[_numberOfSound titleOfSelectedItem] integerValue];
    NSInteger *randomNumber = createPreferredRandomList([tracks count], selectLimit);

    // delete old entities
    [_arrayController removeObjects:[_arrayController arrangedObjects]];

    NSInteger range = (selectLimit < [tracks count]) ? selectLimit : [tracks count];
    for (int i = 0; i < range; i++) {
        iTunesTrack *item = [tracks objectAtIndex:*(randomNumber + i)];

        PSSoundEntity *entity = [_arrayController newObject];

        entity.title = [item name] ?: @"(none)";
        entity.artist = [item artist] ?: [item albumArtist];
        entity.album = [item album] ?: @"(none)";
        entity.played = @NO;
        entity.identifier = @(item.id);
        entity.appendDate = [NSDate date];
        entity.trackid = @([item databaseID]);
    }

    [_tableView deselectAll:nil];
    _currentHistoryCount = 0;

    // save preferences
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref setObject:name forKey:PSPreferenceSelectedLibraryName];
    [pref setObject:[_numberOfSound titleOfSelectedItem] forKey:PSPreferenceNumberOfTrack];

    // free memory
    free(randomNumber);
}

- (IBAction)goNextTrack:(id)sender
{
    NSArray *arrangedObjects = [_arrayController arrangedObjects];
    NSInteger index =[arrangedObjects indexOfObject:_playingEntity];

    _playingEntity = [arrangedObjects objectAtIndex:index + 1];
    if (_playingEntity) {
        [self playTrackForId:_playingEntity.identifier];
        [self appendTrackToBottom:1];

        index = [[_arrayController arrangedObjects] indexOfObject:_playingEntity];
        [_tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:index] byExtendingSelection:NO];
    }
}

- (IBAction)goPreviousTrack:(id)sender
{
    NSArray *arrangedObjects = [_arrayController arrangedObjects];
    NSInteger index =[arrangedObjects indexOfObject:_playingEntity];

    _playingEntity = [arrangedObjects objectAtIndex:index - 1];
    if (_playingEntity) {
        [self playTrackForId:_playingEntity.identifier];

        index = [[_arrayController arrangedObjects] indexOfObject:_playingEntity];
        [_tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:index - 1] byExtendingSelection:NO];
    }
}

- (IBAction)historyCountChange:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:[_history titleOfSelectedItem] forKey:PSPreferenceNumberOfHistory];

    NSArray *arrangedObject = [_arrayController arrangedObjects];
    NSMutableArray *removeTarget = [NSMutableArray array];

    for (PSSoundEntity *entity in arrangedObject) {
        if ([entity.played boolValue]) {
            [removeTarget addObject:entity];
        }
    }

    _currentHistoryCount = 0;

    [_arrayController removeObjects:removeTarget];
    [_tableView reloadDataForRowIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [[_arrayController arrangedObjects] count])]
                          columnIndexes:[NSIndexSet indexSetWithIndex:0]];
}

- (void)iTunesUpdate:(NSNotification *)notification
{
    NSString *playState = [[notification userInfo] objectForKey:@"Player State"];
    NSLog(@"State: %@", playState);

    if ([playState isEqualToString:iTunesPlayerStateStop]) {
        // play next track
        [self goNextTrack:nil];
    }
}

- (IBAction)tableDoubleAction:(id)sender
{
    PSSoundEntity *entity = [[_arrayController selectedObjects] objectAtIndex:0];

    if (_playingEntity == entity || [entity.played boolValue])
        return;

    _playingEntity = entity;
    [self playTrackForId:entity.identifier];

    NSInteger begin = 0;
    NSInteger end = [[_arrayController arrangedObjects] indexOfObject:entity];

    for (PSSoundEntity *e in [_arrayController arrangedObjects]) {
        if ([e.played boolValue])
            begin++;
        else break;
    }

    [self appendTrackToBottom:(end - begin)];
    [_tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:[_arrayController.arrangedObjects indexOfObject:_playingEntity]]
            byExtendingSelection:NO];
}

- (void)playTrackForId:(NSNumber *)identifier
{
    SBElementArray *tracks = [[TNiTunesControler sharedController] tracksForPlaylistName:[_libraryList titleOfSelectedItem]];
    iTunesTrack *track = [tracks objectWithID:identifier];

    [track playOnce:YES];
}

- (void)appendTrackToBottom:(NSInteger)number
{
    if (number == 0)
        return;

    NSArray *arrangedObjects = [_arrayController arrangedObjects];
    // get current index
    NSInteger index = [arrangedObjects indexOfObject:_playingEntity];
    if (index == NSNotFound)
        index = 0;

    NSInteger numOfHistory = [[_history titleOfSelectedItem] integerValue];

    int i;
    _currentHistoryCount = 0;
    NSMutableArray *removeTarget = [[NSMutableArray alloc] init];
    for (i = 0; i < index; i++) {
        PSSoundEntity *entity = [arrangedObjects objectAtIndex:i];
        entity.played = @YES;

        // remove redondent history
        if (_currentHistoryCount == numOfHistory) {
            PSSoundEntity *e = [arrangedObjects objectAtIndex:[removeTarget count]];
            [removeTarget addObject:e];
            _currentHistoryCount--;
        }

        _currentHistoryCount++;
    }

    if ([removeTarget count] != 0)
        [_arrayController removeObjects:removeTarget];

    NSString *sourceName = [_libraryList titleOfSelectedItem];
    NSArray *tracks = [[TNiTunesControler sharedController] tracksForPlaylistName:sourceName];

    if ([tracks count] <= [[_numberOfSound titleOfSelectedItem] integerValue])
        return;

    for (i = 0; i < number; i++) {
        NSInteger select = arc4random() % [tracks count];
        iTunesTrack *item = [tracks objectAtIndex:select];

        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"PSSoundEntity"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identifier == %@", @(item.id)];
        [request setPredicate:predicate];

        NSArray *result = [_appDelegate.managedObjectContext executeFetchRequest:request error:nil];

        if ([result count] != 0) {
            i--;
            continue;
        } else {
            PSSoundEntity *entity = [_arrayController newObject];

            entity.title = [item name] ?: @"(none)";
            entity.artist = [item artist] ?: [item albumArtist];
            entity.album = [item album] ?: @"(none)";
            entity.played = @NO;
            entity.identifier = @(item.id);
            entity.appendDate = [NSDate date];
            entity.trackid = @([item databaseID]);
        }
    }

    [_tableView reloadData];
}

// tableview datasource and delegate
- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    if ([[tableColumn identifier] isEqualToString:@"index"]) {
        return (row < _currentHistoryCount) ? @"" : @(row + 1 - _currentHistoryCount);
    }
    return  nil;
}

- (NSCell *)tableView:(NSTableView *)tableView dataCellForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    PSSoundEntity *entity = [[_arrayController arrangedObjects] objectAtIndex:row];
    NSTextField *cell = [tableColumn dataCellForRow:row];

    [cell setTextColor:([entity.played boolValue]) ? [NSColor grayColor] : [NSColor blackColor]];

    return [tableColumn dataCellForRow:row];
}

@end
