//
//  main.m
//  PartyShaffle
//
//  Created by Tzu-Yi Lin on 2014/2/1.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
