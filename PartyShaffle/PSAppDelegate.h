//
//  PSAppDelegate.h
//  PartyShaffle
//
//  Created by Tzu-Yi Lin on 2014/2/1.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface PSAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (IBAction)saveAction:(id)sender;

@end
