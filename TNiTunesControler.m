//
//  TNiTunesControler.m
//  itunes_notification
//
//  Created by Tzu-Yi Lin on 2014/1/29.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import "TNiTunesControler.h"

@interface TNiTunesControler()
@end

@import ScriptingBridge;


NSString *iTunesTrackChangedNotification = @"com.apple.iTunes.playerInfo";
NSString *iTunesPlayerStatePlaying = @"Playing";
NSString *iTunesPlayerStatePause = @"Paused";
NSString *iTunesPlayerStateStop = @"Stopped";

@implementation TNiTunesControler {
    iTunesApplication *iTunesApp;
}

+ (instancetype)sharedController
{
    static id master = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!master) {
            master = [[self alloc] init];
        }
    });

    return master;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        iTunesApp = [SBApplication applicationWithBundleIdentifier:@"com.apple.iTunes"];
        NSAssert(iTunesApp != nil, @"Cannot Get iTunes instance");
        if (![iTunesApp isRunning])
            [iTunesApp run];
    }
    return self;
}

- (SBElementArray *)libraryPlaylists
{
    NSArray *sources = [iTunesApp sources];
    iTunesSource *library = nil;

    for (iTunesSource *item in sources) {
        if (item.kind == iTunesESrcLibrary) {
            library = item;
            break;
        }
    }

    return [library playlists];
}

- (SBElementArray *)tracksForPlaylistName:(NSString *)name
{
    iTunesPlaylist *playlist = [[self libraryPlaylists] objectWithName:name];
    return [playlist tracks];
}

- (BOOL)iTunesPlaying
{
    return [iTunesApp playerState] == iTunesEPlSPlaying;
}

- (iTunesTrack *)currentPlayingTrack
{
    return ([self iTunesPlaying]) ? iTunesApp.currentTrack :  nil;
}

@end

