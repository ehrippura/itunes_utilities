//
//  main.m
//  albumrate
//
//  Created by Wayne Lin on 2016/2/25.
//  Copyright © 2016年 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h>

// created by console command: sdef /Applications/iTunes.app | sdp -fh --basename iTunes
#import "../iTunes.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        iTunesApplication *app = [SBApplication applicationWithBundleIdentifier:@"com.apple.iTunes"];
        if (!app.isRunning) {
            [app run];
        }

        iTunesSource *source = nil;

        for (iTunesSource *s in [app sources] ) {
            if (s.kind == iTunesESrcLibrary) {
                source = s;
                break;
            }
        }

        iTunesPlaylist *library = source.libraryPlaylists.firstObject;

        for (iTunesTrack *track in library.tracks) {
            if (track.albumRating != 0 && track.albumRatingKind == iTunesERtKUser) {
                NSLog(@"%@ - %@", track.album, track.name);
                track.albumRating = 0;
            }
        }
    }
    return 0;
}
