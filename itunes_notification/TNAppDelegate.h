//
//  TNAppDelegate.h
//  itunes_notification
//
//  Created by Tzu-Yi Lin on 2014/1/27.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TNAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
