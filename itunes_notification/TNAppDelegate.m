//
//  TNAppDelegate.m
//  itunes_notification
//
//  Created by Tzu-Yi Lin on 2014/1/27.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import "TNAppDelegate.h"
#import "../iTunes.h"

#import <ScriptingBridge/ScriptingBridge.h>

@interface TNAppDelegate () {
    IBOutlet NSTextField *_titleLabel;
    IBOutlet NSTextView *_textView;
    IBOutlet NSMenu *_menu;

    NSStatusItem *_statusItem;
}

- (void)updateInfo;

@end

@implementation TNAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSStatusBar *systemStatusBar = [NSStatusBar systemStatusBar];
    _statusItem = [systemStatusBar statusItemWithLength:35];
    [_statusItem setTitle:@"iTN"];
    [_statusItem setHighlightMode:YES];
    [_statusItem setMenu:_menu];

    NSDistributedNotificationCenter *
    des = [NSDistributedNotificationCenter defaultCenter];

    [des addObserver:self selector:@selector(iTunesUpdate:) name:@"com.apple.iTunes.playerInfo" object:nil];
    [self updateInfo];
}

- (void)updateInfo
{
    iTunesApplication *itunes = (iTunesApplication*)[SBApplication applicationWithBundleIdentifier:@"com.apple.iTunes"];
    iTunesTrack *track = [itunes currentTrack];

    if ([[track lyrics] length] != 0) {
        [_titleLabel setStringValue:[NSString stringWithFormat:@"%@ - %@", [track artist], [track name]]];
        [_textView setString:[track lyrics]];

        [_textView scrollToBeginningOfDocument:nil];
        [self.window makeKeyAndOrderFront:nil];
    } else {
        [self.window orderOut:nil];
    }
}

- (void)iTunesUpdate:(NSNotification *)notification
{
    [self updateInfo];
}

@end
