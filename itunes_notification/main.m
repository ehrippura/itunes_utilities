//
//  main.m
//  itunes_notification
//
//  Created by Tzu-Yi Lin on 2014/1/27.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
